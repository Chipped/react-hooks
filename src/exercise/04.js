/* eslint-disable react-hooks/exhaustive-deps */
// useState: tic tac toe
// http://localhost:3000/isolated/exercise/04.js

import * as React from 'react'
import {flushSync} from 'react-dom'
import useLocalStorageState from './useLocalStorageState.js'

function Board() {
  // 🐨 squares is the state for this component. Add useState for squares
  const [squares, setSquares] = useLocalStorageState(
    'squares',
    Array(9).fill(null),
  )
  const [winner, setWinner] = useLocalStorageState('winner', null)
  const [status, setStatus] = useLocalStorageState('status', null)
  const [currentStep, setCurrentStep] = useLocalStorageState('currentStep', null);
  
  const [history, setHistory] = useLocalStorageState([]);

  const firstRowRef = React.useRef(null)

  // 🐨 We'll need the following bits of derived state:
  // - nextValue ('X' or 'O')
  // - winner ('X', 'O', or null)
  // - status (`Winner: ${winner}`, `Scratch: Cat's game`, or `Next player: ${nextValue}`)
  // 💰 I've written the calculations for you! So you can use my utilities
  // below to create these variables

  React.useEffect(() => {
    // check if this move is a winner
    const winnerVal = calculateWinner(squares)
    if (winnerVal) {
      setWinner(winnerVal);
    } else if (winner) {
      setWinner(false);
    }

    setStatus(
      calculateStatus(winnerVal, squares, calculateNextValue(squares)),
    )
  }, [squares])

  // This is the function your square click handler will call. `square` should
  // be an index. So if they click the center square, this will be `4`.
  function selectSquare(square) {
    console.log({history})
    if (squares[square] || winner) {
      return
    }

    // console.log('going for it')

    const squaresArr = [...squares]

    squaresArr[square] = calculateNextValue(squaresArr);

    setSquares(squaresArr)
    setHistory((h) => [ ...h, squares ]);
  }

  function restart() {
    // 🐨 reset the squares
    // 💰 `Array(9).fill(null)` will do it!
    const defaultSquares = Array(9).fill(null)

    setSquares(defaultSquares)
    setHistory([]);
  }

  function undo() {
    if (!history.length) {
      return;
    }

    const historyArr = [ ...history ];

    setSquares(history[history.length - 1]);
    console.log('undonig, ', {history })

    historyArr.pop();

    setHistory(historyArr);
  }

  function renderSquare(i) {
    return (
      <button className="square" onClick={() => selectSquare(i)}>
        {squares[i]}
      </button>
    )
  }

  return (
    <div>
      {/* 🐨 put the status in the div below */}
      <div className="status">STATUS: {status}</div>
      <div className="board-row" ref={firstRowRef}>
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
      <button className="restart" onClick={restart}>
        Restart
      </button>
      <ol>
        <li>
          <button onClick={() => setSquares(Array(9).fill(null))}>
            Go to game start
          </button>
        </li>
        {history.map((h, idx) => (
          <li key={idx}>
            <button className='history-btn' disabled={JSON.stringify(h) === JSON.stringify(squares)} onClick={() => setCurrentStep(idx)}>
              Go to move #{idx + 1}
            </button>
          </li>
        ))}
      </ol>
    </div>
  )
}

function Game() {
  return (
    <div className="game">
      <div className="game-board">
        <Board />
      </div>
    </div>
  )
}

// eslint-disable-next-line no-unused-vars
function calculateStatus(winner, squares, nextValue) {
  return winner
    ? `Winner: ${winner}`
    : squares.every(Boolean)
    ? `Scratch: Cat's game`
    : `Next player: ${nextValue}`
}

// eslint-disable-next-line no-unused-vars
function calculateNextValue(squares) {
  return squares.filter(Boolean).length % 2 === 0 ? 'X' : 'O'
}

// eslint-disable-next-line no-unused-vars
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i]
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a]
    }
  }
  return null
}

function App() {
  return <Game />
}

export default App
