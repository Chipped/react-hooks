import * as React from 'react'

export default function useLocalStorageState(
  key,
  defaultValue = '',
  {serialize = JSON.stringify, deserialize = JSON.parse} = {},
) {
  const [value, setValue] = React.useState(() => {
    const storedData = window.localStorage.getItem(key)

    if (storedData) {
      return deserialize(storedData) || ''
    }

    return typeof defaultValue === 'function' ? defaultValue() : defaultValue
  })

  let prevKeyRef = React.useRef(key)

  React.useEffect(() => {
    const prevKey = prevKeyRef.current
    if (prevKey !== key) {
      window.localStorage.removeItem(prevKey)
    }
    prevKeyRef.current = key

    window.localStorage.setItem(key, serialize(value))
  }, [value, serialize, deserialize, key])

  return [value, setValue]
}
